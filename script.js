/* Теоритичні питання:
1. В чому полягає відмінність localStorage і sessionStorage?
   Основна відмінність між localStorage і sessionStorage полягає в тривалості зберігання даних та їх доступності між вкладками браузера.

2. Які аспекти безпеки слід враховувати при збереженні чутливої інформації, такої як паролі, за допомогою localStorage чи sessionStorage?
   При збереженні чутливої інформації в localStorage чи sessionStorage важливо шифрувати дані, уникати зберігання паролів у відкритому форматі, контролювати доступ до даних, використовувати безпечне SSL/TLS з'єднання та регулярно оновлювати та моніторити безпеку.

3. Що відбувається з даними, збереженими в sessionStorage, коли завершується сеанс браузера?
   Коли завершується сеанс браузера, дані, збережені в sessionStorage, автоматично видаляються, оскільки sessionStorage призначений для зберігання даних тільки на час життя поточної сесії браузера.


Практичне завдання:

Реалізувати можливість зміни колірної теми користувача.

Технічні вимоги:

- Взяти готове домашнє завдання HW-4 "Price cards" з блоку Basic HMTL/CSS.
- Додати на макеті кнопку "Змінити тему".
- При натисканні на кнопку - змінювати колірну гаму сайту (кольори кнопок, фону тощо) на ваш розсуд. 
    При повторному натискання - повертати все як було спочатку - начебто для сторінки доступні дві колірні теми.
- Вибрана тема повинна зберігатися після перезавантаження сторінки.

Примітки: 
- при виконанні завдання перебирати та задавати стилі всім елементам за допомогою js буде вважатись помилкою;
- зміна інших стилів сторінки, окрім кольорів буде вважатись помилкою.

Додаткові матеріали: https://developer.mozilla.org/en-US/docs/Web/CSS/Using_CSS_custom_properties.
*/

const themeCheckbox = document.getElementById('theme');
const cssLink = document.getElementById('theme-css');

const DARK_CSS = './css/dark.css'
const LIGHT_CSS = './css/light.css'

themeCheckbox.addEventListener('click', (event) => {

    if(event.target.checked){
        cssLink.setAttribute('href', DARK_CSS)
        localStorage.setItem('theme', 'dark')
    }
    else {
        cssLink.setAttribute('href', LIGHT_CSS)
        localStorage.setItem('theme', 'light')
    }
})


const lastTheme = localStorage.getItem('theme') ?? 'light'

if (lastTheme == 'light'){
    cssLink.setAttribute('href', LIGHT_CSS)
}
else {
    cssLink.setAttribute('href', DARK_CSS)
    themeCheckbox.checked = true;
}
